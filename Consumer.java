//Vaibhav Patel
public class Consumer {

	protected String conName;

	protected String categoryName;
	
	public Consumer(String conName, String categoryName, String s) 
	{
		//@Begin
		
		this.conName=conName;
		this.categoryName=categoryName;
		
		if(s.equals("Subscribe"))													// Consumer Subscribes
		{
			Filter_Broker fb=new Filter_Broker();
			fb.addConsumer(this);
		}
		else																// Consumer UnSubscribes
		{
			Filter_Broker fb=new Filter_Broker();				
			fb.removeConsumer(this);
		}

		//@End
	}

	public static void update(String conName, String productName, String vendorName) 
	{
		//@Begin
		
		System.out.println(conName+" notified of "+productName+" from "+vendorName);	// Consumer is updated of notification
		
		//@End
	}

}
