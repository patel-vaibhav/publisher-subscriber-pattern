//Vaibhav Patel
public class Vendor {

	protected String vendorName;

	protected String productName;

	protected String categoryName;

	public Vendor(String vendorName, String categoryName, String productName) 	//Vendor Constructor
	{
		//@Begin
		this.vendorName=vendorName;
		this.productName=productName;
		this.categoryName=categoryName;
		Filter_Broker nf=new Filter_Broker();
		nf.notify(Vendor.this);
		//@End
	}
}
