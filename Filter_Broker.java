//Vaibhav Patel
import java.util.Dictionary;
import java.util.ArrayList;
import java.util.Hashtable;

public class Filter_Broker {

	private static Dictionary<String,ArrayList<String>> consumerList=new Hashtable<String, ArrayList<String>>();

		
	public void addConsumer(Consumer newConsumer) 								// method to add consumers to the subscription List
	{
		//@Begin
		String category=newConsumer.categoryName;
		String consumerName=newConsumer.conName;
		
	    	if (consumerList.get(newConsumer.categoryName) != null)
	    	{
	          	if (!consumerList.get(category).contains(consumerName))
	          	{
	        	  	consumerList.get(category).add(consumerName);         
	    		}
	    	}		
	    	else
	    	{
	          	ArrayList<String> newSubscribersList = new ArrayList<String>();
	          	newSubscribersList.add(consumerName);
	          	consumerList.put(category,newSubscribersList);
	    	}
		//@End
	}

	
	public void removeConsumer(Consumer thisConsumer) 									// Method to remove consumer from subscription list
	{
		//@Begin
		
		String category=thisConsumer.categoryName;
		String consumerName=thisConsumer.conName;
		
		if (consumerList.get(category) != null)
	      	{
	          	if (consumerList.get(category).contains(consumerName))
	          	{
	        	  	consumerList.get(category).remove(consumerName);
	          	}
	      	}
		//@End
	}

	
	private ArrayList<String> getConsumers(String categoryName) 				// Method which returns arrayList of consumers subscribed for particular category
	{
		//@Begin
	    	ArrayList<String> notifylist1=new ArrayList<String>();

	    	if (consumerList.get(categoryName) != null)
	    	{
	        	notifylist1=(ArrayList<String>)consumerList.get(categoryName);
	    	}
	    	return notifylist1;
		//@End
	}

	
	public void notify(Vendor vendorObj) 							// Method which notifies consumers of the vendor product
	{
		//@Begin
		ArrayList<String> notifylist;
		if(!getConsumers(vendorObj.categoryName).equals(null))
		{
			notifylist=(ArrayList<String>)getConsumers(vendorObj.categoryName);
			
			for(int i=0;i<notifylist.size();++i)
			{
				Consumer.update(notifylist.get(i),vendorObj.productName,vendorObj.vendorName);
			}
			
		}
		//@End
	}

}
