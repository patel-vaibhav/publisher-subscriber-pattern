//Vaibhav Patel
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Input 
{

	protected static Vendor vendorObj;

	protected static Consumer consumerObj;

	public static void main(String args[]) throws IOException,FileNotFoundException
	{
		//@Begin
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		try
		{
			String data;
			while((data=br.readLine()) != null)
			{
				data=data.toLowerCase();									// Convert whole file into lower case 
				data=data.trim();
				String str1[]=data.split(",");
				
				for(int i=0;i<str1.length;i++)
				{
					str1[i]=str1[i].trim();
				}
				if(str1[0].toString().equals("subscribe"))							// Create object of Consumer on command subscribe
				{
					consumerObj=new Consumer(str1[1],str1[2],"Subscribe");
					consumerObj=null;
				}
				if(str1[0].toString().equals("unsubscribe"))						
				{
					consumerObj=new Consumer(str1[1],str1[2],"Unsubscribe");
					consumerObj=null;
				}
				if(str1[0].toString().equals("publish"))							// Create object of Vendor on command publish
				{
					vendorObj=new Vendor(str1[1],str1[2],str1[3]);
					vendorObj=null;
				}
			}
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
	    	 	e.printStackTrace();
		}
		finally
		{
			br.close();	
		}
		//@End
	}

	

}
